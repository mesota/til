## factory_bot

### ファクトリの定義
ファクトリのクラスはファクトリ名から推測される
```ruby
FactoryBot.define do
  # 推測されるクラスはUserクラス
  factory user do
    name { Test }
  end
end
```
クラス名と違うファクトリ名を付ける場合は、クラスを明示する必要がある
```ruby
factory actor, class: user do
```
古いRailsで利用している場合は、シンボル、文字列でクラス名を指定することもできる
```ruby
factory :access_token, class: "Doorkeeper::AccessToken"
```

### ファクトリの読み込み
以下のディレクトリ配下のファクトリは自動的に読み込まれる
```
test/factories.rb
spec/factories.rb
test/factories/*.rb
spec/factories/*.rb
```

### ファクトリの使い方
```ruby
# 保存されていないインスタンスを返す
user = build(:user)
# 保存されたインスタンスを返す
user = create(:user)
# ファクトリに定義された属性のハッシュを返す
user_attrs = attributes_for(:user)
# スタブを返す
user_stub = build_stubbed(:user)
# 上記のメソッドにブロックを渡すこともできる
# createの場合
create(:user) do |user|
  user.name = "Test"
end
```
ファクトリの属性定義を上書きすることもできる
```ruby
# 名前を上書き
user = create(:user, name: "Test")
```

### 静的な属性
静的な属性（ブロックではない）は`factory_bot 5`から廃止された
```ruby
factory :user do
  name "John"
end
```

### 他の属性に依存した属性
属性値には他の属性値を含めることができる
```ruby
factory :user do
  name { "John" }
  email { "#{name}@example.com" } 
end
```

### 一時的な属性
クラス定義に依存しない一時的な属性を定義できる
```ruby
factory :user do
  transient do
    rokster { true }
    admin { false }
  end
end

user = create(:user, admin: true)
```
一時的な属性は`attributes_for`では無視される

### 予約語やメソッド名を使用する
`add_attribute`メソッドを使用すると可能
```ruby
factory :user do
  # 予約語
  add_attribute(:method)
  # メソッド名
  add_attribute(:sequence) { "aaa" }
end
```

### エイリアス
ファクトリ名には別名（エイリアス）を設定できる
```ruby
factory :user aliases: [:auther, :admin] do
  name { "John" }
end
```

 ### ファクトリの継承
 ファクトリ定義を入れ子にすることで継承することができる
 ```ruby
 factory :user do
  name { "John" }
  email { "John@example.com" }
  factory :admin do
    admin { true }
  end
 end

 admin = create(:admin)
 admin.name # => "John"
 ```
継承元のファクトリを明示的に指定する場合
```ruby
factory :user do
  name { "John" }
  email { "John@example.com" }
end

factory :admin parent: :user do
  admin { true }
end
```

### ファクトリの関連付け

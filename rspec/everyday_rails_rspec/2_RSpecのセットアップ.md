## 2.RSpecのセットアップ

### Gemの追加
`Gemfile`{の`test`グループに`rspec-rails`を追加する
```ruby
group :test do
  gem 'rspec-rails', '~>3.8'
end
```

### 設定ファイルのボイラープレートを作成
```
$ bin/rails g rspec:install
```

### binstubを作成
Springを使ってrspecのプリロードを行う

gemを追加し
、
```ruby
group :development do
  gem 'spring-commands-rspec'
end
```
コマンドを実行
```
$ bundle exec spring binstub rspec
```
`bin`ディレクトリに`rspec`という名前の実行ファイルが作成される  
以下のコマンドでbinstubを利用して実行する
```
$ bin/rspec ...
```

### ジェネレータの設定
`bin/rails g rspec:...`コマンドで作成されるファイルを制御する
```ruby
# config/application.rb
module SampleApp
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    ...

    # rspecのジェネレータ設定
    config.generators do |g|
      g.test_framework :rspec,
        # フィクスチャを作成しない
        fixtures: false,
        # ビュースペックを作成しない
        view_specs: false,
        # ヘルパースペックを作成しない
        helper_specs: false,
        # ルーティングスペックを作成しない
        routing_specs: false
    end
  end
end
```
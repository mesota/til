## 6.フィーチャースペックでUIをテストする。

### フィーチャースペック
ソフトウェア全体が一つのシステムとして期待通りに動くことを検証する。  
(Rails5.1ではシステムテストとして同様のテストがデフォルトでサポートされている。)

### [Capybara](https://github.com/teamcapybara/capybara)
ブラウザ操作をシミュレートすることができる。  
(Rails5.1ではデフォルトでインストールされている)

### 作成コマンド
```
rails generate rspec:feature <フィーチャー名>
```
`spec/features/`配下にファイルが作成される

### 基本構文
```ruby
RSpec.feature "<フィーチャー名>", type:feature do
    scenario "" do
        # Capybara DSL やrspecの記法でテストを書く
    end
end
```

### Capybara DSL
Capybaraはブザウザ上の操作をシミュレートするさまざまなメソッドを提供している。  
[CapybaraのDSL](https://github.com/teamcapybara/capybara#the-dsl)

#### click_buttonを使う場合の注意点
起動されたアクションが完了する前に次の処理へ移ってしまう場合があるため、click_buttonを実行したexpectの内部で最低でも一回以上のエクスペくテーションを実施する必要がある。  
例)  
```ruby
expect {
    ...
    click_button "Create Project"
    ...
    expect(page).to have_content "Project was successfully created"
}
```

### デバッグ
`Capybara`はデフォルトでヘッドレスブラウザ(UIを持たないブラウザ)でテストを実行する。  
`save_and_open_page`を利用すると、テスト時のGUIページを確認することができる。
例)  
```ruby
scenario "guest adds a project" do
    visit projects_path
    # GUIページがhtml形式で保存される
    save_and_open_page
    click_link "New Project"
end
```

### JavaScriptを使った操作のテスト
`Capybara`のデフォルトで実行されるドライバ(`Rack::Test`)は`JavaScript`の実行をサポートしていない  
`scenario`に`js: true`オプションを付与してJavaScriptの実行をサポートしているドライバでの実行を指定する  
デフォルトのドライバは`selenium-webdriver(gem)`
``` ruby
scenario "***", js: ture do
```

### ヘッドレスドライバ
32bit ubuntu環境ではchromeが使用できないのでPhantomJSを使用する  
### PhantomJSのインストール
1. PhantomJSパッケージをインストール
`sudo apt-get install phantomjs`
2. バイナリファイルを[ダウンロード(32bit)](https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-i686.tar.bz2)
3. ダウンロードしたファイルを解凍し、`bin/phantomjs`をRailsフォルダの`bin`フォルダにコピーする
4. `spec/support/capybara.rb`を以下に変更
```ruby
require 'capybara/poltergeist'
Capybara.javascript_driver = :poltergeist
```
5. `Gemfile`に`poltergeist`を追加し、gemをインストールする

### JavaScriptの終了を待つ
Capybaraはデフォルトで2秒間JavaScriptの終了を待機する
`spec/support/capybara.rb`に以下の設定をすることでスペック全体で待機時間を変更できる  
```ruby
Capybara.default_max_wait_time = 15
```






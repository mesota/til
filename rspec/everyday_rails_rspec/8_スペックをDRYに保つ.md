## 8.スペックをDRYに保つ

### サポートモジュール
共通部分をサポートモジュールとして別ファイルに切り出す。
```ruby
# spec/support/login_support.rb
module LoginSupport
  def sign_in_as(user)
    #共通のログイン処理
  end
end
```
#### RSpec全体で利用する処理の場合
サポートモジュール内に`include`処理を記述する。
```ruby
# spec/support/login_support.rb
# RSpecにinclude
RSpec.configure do |config|
  config.include LoginSupport
end
```

#### 特定のスペックのみで利用する処理の場合
個別に`include`する。
```ruby
RSpec.feature "Projects", type: :feature do
  include LoginSupport
  ...
end
```

### 遅延読み込み
`let`を利用してデータを遅延読み込みで作成する。  
`before`はテストが実行される前にその都度実行される、`let`は呼び出されるまで実行されない。
```ruby
let(:project) { FactoryBot.create(:project) }
```
`let!`で定義と同時にデータを作成することもできる。

### contextの共有
`shared context`を使ってコンテキストを共有することができる。
```ruby
# spec/support/context/project_setup.rb
RSpec.shared_contextn "project setup" do
  let(:user) { FactoryBot.create(:user) }
  let(:project) { FactoryBot.create(:project, owner: user) }
  let(:task) { project.tasks.create!(name: "Test task") }
end
```
```ruby
# 使用するスペックでincludeする
include_context "project setup"
```

### カスタムマッチャ
独自のマッチャを定義することで、スペックで共通の記述を減らすことができる。  
[注意]マッチャを作成する場合、メンテナンスコストに見合うかよく考える。  
`shoulda-matchers gem`などの既存の`gem`の使用を検討する。
```ruby
RSpec::Matchers.define :have_content_type do |expected|
  # 必須メソッド
  match do |actual|
    ...
  end

  # to の場合のエラーメッセージ
  failure_message do |actual|
    ...
  end

  # to not の場合のエラーメッセージ
  failure_message_when_negated do |actual|
    ...
  end
end

# マッチャのエイリアス設定
RSpec::Matchers.alias_matcher :be_content_type, :have_content_type
```

### 失敗エクスペクテーションの集約
複数のエクスペクテーションを集約して、たとえそのうちの一つが失敗しても全てのエクスペクテーションが実行される。
```ruby
aggregate_failures do
  expect ... #1つ目のエクスペクテーション
  expect ... #2つ目のエクスペクテーション
end
```
```
# エラー出力
Failures:
  ...
  1) ...
  ...
    1.1) 1つ目のエクスペクテーション
    ...
    1.2) 1つ目のエクスペクテーション
    ...
```

### ヘルパーメソッドを使った抽象化
スペックの各ステップをヘルパーメソッドに抽出することで可読性を向上させることができる。(シングルレベルの抽象化)  
[注意]必要以上の抽象化は可読性を向上させる上で逆効果になる。
